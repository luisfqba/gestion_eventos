<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class InscripcionForm extends CFormModel
{
	//atributos Cuenta                      
        public $idcuenta;
        public $username;
        public $password;
        public $fechaalta;
        public $rol;
        //atributos Usuario
        public $nombre;
        public $apellido1;
        public $apellido2;
        public $email;
        public $direccion;
        public $ciudad;
        public $pais;
        public $codigopostal;
        public $telefono;
        public $compania;
        //atributos de la tabla pivote inscripcion
        //public $idinscripcion;
        //public $idevento;
        //public $idusuario;
        public $claveacceso;
        public $cantparticipantes;
        public $codbanco;
        public $idevento;
        //atributos de hotel    
        public  $idhotel;
        public  $idhabitacion;
        


        //atributo capcha
        public $verifyCode;
        
        public $error_html;
               
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			//array('nombre, apellido1, apellido2, email, direccion, ciudad, pais, codigopostal, telefono, compania, idcuenta', 'required'),
                        array('username,password,fechaalta,rol,nombre, apellido1, apellido2, email, direccion, ciudad, pais, codigopostal, telefono, compania, claveacceso, cantparticipantes, codbanco, idevento, idhabitacion', 'required'),
			array('idcuenta', 'numerical', 'integerOnly'=>true),
			array('nombre, apellido1, apellido2', 'length', 'max'=>40),
                        array('email, idcuenta, username','unique'),
                        // email has to be a valid email address
			array('email', 'email'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idusuario, nombre, apellido1, apellido2, email, direccion, ciudad, pais, codigopostal, telefono, compania, idcuenta', 'safe', 'on'=>'search'),
                      
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>'Verification Code',
		);
	}
        
        public function crearObjetos(){               
               $tr = Yii::app()->db->beginTransaction();
             
               $_cuenta = new Cuenta;
               $_cuenta->username = $this->username;
               $_cuenta->password = md5($this->password);
               $_cuenta->fechaalta = date("Y-m-d");
               $_cuenta->rol = 'usuario';                   
               if(!$_cuenta->insert()){ 
                  $this->error_html = CHtml::errorSummary($_cuenta);
                   $tr->rollback();    // se cancela la transaccion
                   return false;        // porque el cliente no se pudo crear
               }               
       
               $_usuario = new Usuario;
               $_usuario->nombre = $this->nombre; 
               $_usuario->apellido1 = $this->apellido1;
               $_usuario->apellido2 = $this->apellido2;
               $_usuario->email = $this->email;
               $_usuario->direccion = $this->direccion;
               $_usuario->ciudad = $this->ciudad;
               $_usuario->pais = $this->pais;
               $_usuario->codigopostal = $this->codigopostal;
               $_usuario->telefono = $this->telefono;
               $_usuario->compania = $this->compania;
               $_usuario->idcuenta = $_cuenta->idcuenta;

               if( false == $_usuario->insert()){
                    $this->error_html = CHtml::errorSummary($_usuario);
                    $tr->rollback();
                    return false;
               }
               //registrando la Inscripcion
               $_inscripcion = new Inscripcion();
               $_inscripcion->idevento = $this->idevento;
               $_inscripcion->idusuario = $_usuario->idusuario;
               $_inscripcion->aprobada = false;
               $_inscripcion->claveacceso = "123";
               $_inscripcion->codbanco = "1233";
               $_inscripcion->canparticipantes = 0;
               if($_inscripcion->canparticipantes != null)
                   $_inscripcion->canparticipantes = $this->cantparticipantes;
                   
               //$_inscripcion->fechaalta = date("Y-m-d");  
            
               if( false == $_inscripcion->insert()){
                    $this->error_html = CHtml::errorSummary($_inscripcion);
                    $tr->rollback();
                    return false;
               }
               
               //registrando la Reservacion
               $_reservacion = new Reservacion();
               $_reservacion->idhabitacion = $this->idhabitacion;
               $_reservacion->idusuario = $_usuario->idusuario;
               //aki el usuario tiene que coger la fecha de entrada y salida
               $_reservacion->fechaentrada = date("Y-m-d");
               $_reservacion->fechasalida = date("Y-m-d");
                            
               if( false == $_reservacion->insert()){
                    $this->error_html = CHtml::errorSummary($_reservacion);
                    $tr->rollback();
                    return false;
               }
               $tr->commit();              
         }        
       
}