<?php

/**
 * This is the model class for table "public.paquete".
 *
 * The followings are the available columns in table 'public.paquete':
 * @property integer $idpaquete
 * @property string $nombre
 * @property string $descripcion
 * @property integer $idevento
 *
 * The followings are the available model relations:
 * @property Cuota[] $cuotas
 * @property Descuento[] $descuentos
 * @property Evento $idevento
 */
class Paquete extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.paquete';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, descripcion, idevento', 'required'),
			array('idevento', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idpaquete, nombre, descripcion, idevento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cuotas' => array(self::HAS_MANY, 'Cuota', 'idpaquete'),
			'descuentos' => array(self::HAS_MANY, 'Descuento', 'idpaquete'),
			'idevento' => array(self::BELONGS_TO, 'Evento', 'idevento'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idpaquete' => 'Idpaquete',
			'nombre' => 'Nombre',
			'descripcion' => 'Descripcion',
			'idevento' => 'Idevento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idpaquete',$this->idpaquete);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('idevento',$this->idevento);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Paquete the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
