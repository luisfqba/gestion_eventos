<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',
        
	// preloading 'log' component
	'preload'=>array('log'),
    
        'language'=>'es', 

        //insertado el array alias para el boostrap
        'aliases' => array(
                'bootstrap' => 'application.modules.bootstrap',
                'chartjs' => 'application.modules.bootstrap.extensions.yii-chartjs-master'
        ),        
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
            //insertado por mi para el boostrap
                'bootstrap.*',
                'bootstrap.components.*',
                'bootstrap.models.*',
                'bootstrap.controllers.*',
                'bootstrap.helpers.*',
                'bootstrap.widgets.*',
                'bootstrap.extensions.*',
                'chartjs.*',
                'chartjs.widgets.*',
                'chartjs.components.*',
            //hasta aki lo del boostrap
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'postgres',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
                        //insertado para el boostrap
                        //'generatorPaths' => array('bootstrap.gii'),
		),
                //insertado para el boostrap
                 'bootstrap' => array(
                                'class' => 'bootstrap.BootStrapModule'
                 ),
		 //hasta aki lo del boostrap
	),
        
	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
                //insertado para el boostrap
                'bootstrap' => array(
                        'class' => 'bootstrap.components.BsApi'
                    ),
                'bsHtml' => array('class' => 'bootstrap.components.BSHtml'),
                'chartjs'=>array('class' => 'chartjs.components.ChartJs'),
                //hasta aki lo del boostrap
		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			//'connectionString' => 'mysql:host=localhost;dbname=testdrive',
                        'connectionString' => 'pgsql:host=localhost;dbname=sgseventos',
			'emulatePrepare' => true,
			'username' => 'postgres',
			'password' => 'postgres',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);