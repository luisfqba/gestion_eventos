<?php
/* @var $this TematicaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tematicas',
);

$this->menu=array(
	array('label'=>'Create Tematica', 'url'=>array('create')),
	array('label'=>'Manage Tematica', 'url'=>array('admin')),
);
?>

<h1>Tematicas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
