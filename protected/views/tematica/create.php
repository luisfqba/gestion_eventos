<?php
/* @var $this TematicaController */
/* @var $model Tematica */

$this->breadcrumbs=array(
	'Tematicas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tematica', 'url'=>array('index')),
	array('label'=>'Manage Tematica', 'url'=>array('admin')),
);
?>

<h1>Create Tematica</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>