<?php
/* @var $this InscripcionController */
/* @var $model InscripcionForm */
/* @var $form InscripcionForm */

$this->pageTitle=Yii::app()->name . ' - Inscripcion';
$this->breadcrumbs=array(
	'Inscripcion',
);
?>

<h1>Inscripcion</h1>

<?php if(Yii::app()->user->hasFlash('inscripcion')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('inscripcion'); ?>
</div>

<?php else: ?>

<p>
If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
</p>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'inscripcion-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->textField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
        
       <div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apellido1'); ?>
		<?php echo $form->textField($model,'apellido1',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'apellido1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apellido2'); ?>
		<?php echo $form->textField($model,'apellido2',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'apellido2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textField($model,'direccion'); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ciudad'); ?>
		<?php echo $form->textField($model,'ciudad'); ?>
		<?php echo $form->error($model,'ciudad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pais'); ?>
		<?php echo $form->textField($model,'pais'); ?>
		<?php echo $form->error($model,'pais'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'codigopostal'); ?>
		<?php echo $form->textField($model,'codigopostal'); ?>
		<?php echo $form->error($model,'codigopostal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono'); ?>
		<?php echo $form->textField($model,'telefono'); ?>
		<?php echo $form->error($model,'telefono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'compania'); ?>
		<?php echo $form->textField($model,'compania'); ?>
		<?php echo $form->error($model,'compania'); ?>
	</div>
        <!-- Combos dependientes... -->    
                
        <div class="row">
            <?php 
            $lista_dos = array();
                //este id tiene que venir del modelo es al q el usuario querra inscribirse
                $id_uno = intval($model->idevento);                 
                
                $lista_dos = CHtml::listData(Hotel::model()->findAll("idevento = '$id_uno'"),'idhotel','nombre');
               // }                
                echo $form->dropDownList($model,'idhotel',$lista_dos,
                        array(
                            'ajax'=>array(
                              'type'=>'POST',
                              'url'=>CController::createUrl('Usuario/cargarhabitaciones'),
                              'update'=>'#'.CHtml::activeId($model,'idhabitacion'),
                              'beforeSend' => 'function(){
                                  $("#InscripcionForm_idhabitacion").find("option").remove();
                                  $("#nivel_tres").hide("fast");
                               }',                                  
                              'complete' => 'function(){
                                   $("#nivel_tres").show("fast");
                              }',  
                            ),                            
                            'prompt'=>'Seleccione')
                        );             
                echo $form->error($model,'idhotel');            
            ?>
                
        </div>
        
        <div class="row" id='nivel_tres' style="display:none;">
		<?php echo $form->labelEx($model,'idhabitacion'); ?>
		<?php 
                $lista_tres = array();
                if(isset($model->idhabitacion)){
                $id_dos = intval($model->idhotel); 
                $lista_tres = CHtml::listData(Habitacion::model()->findAll("idhotel = '$id_dos'"),'idhabitacion','numero');
                }
                echo $form->dropDownList($model,'idhabitacion',$lista_tres,
                        array('prompt'=>'Seleccione')
                        ); ?>
		<?php echo $form->error($model,'idhabitacion'); ?>
	</div>

        
        <?php if(CCaptcha::checkRequirements()): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<div>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>
		<div class="hint">Please enter the letters as they are shown in the image above.
		<br/>Letters are not case-sensitive.</div>
		<?php echo $form->error($model,'verifyCode'); ?>
	</div>
	<?php endif; ?>      

        
	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php endif; ?>