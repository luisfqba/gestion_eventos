<?php
/* @var $this PaqueteController */
/* @var $model Paquete */

$this->breadcrumbs=array(
	'Paquetes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Paquete', 'url'=>array('index')),
	array('label'=>'Manage Paquete', 'url'=>array('admin')),
);
?>

<h1>Create Paquete</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>