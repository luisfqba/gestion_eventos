<?php
/* @var $this PaqueteController */
/* @var $data Paquete */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idpaquete')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idpaquete), array('view', 'id'=>$data->idpaquete)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idevento')); ?>:</b>
	<?php echo CHtml::encode($data->idevento); ?>
	<br />


</div>