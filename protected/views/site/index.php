<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>
<h2><?php echo '<h1>'.$saludo.'</h1>';?></h2>
<p>Congratulations! You have successfully created your Yii application.</p>

<p>You may change the content of this page by modifying the following two files:</p>
<ul>
	<li>View file: <code><?php echo __FILE__; ?></code></li>
	<li>Layout file: <code><?php echo $this->getLayoutFile('main'); ?></code></li>
</ul>
<table>
    <tr>   
      <b>Titulo</b>
    </tr>
    <?php foreach ($libros as $value): ?>
    <tr>    
        <td>
            <?php echo $value;?>
        </td>
        <td>
            <?php echo 'Del';?>
        </td>
     </tr>
    <?php endforeach; ?>
   
</table>

<p>For more details on how to further develop this application, please read
the <a href="http://www.yiiframework.com/doc/">documentation</a>.
Feel free to ask in the <a href="http://www.yiiframework.com/forum/">forum</a>,
should you have any questions.</p>
