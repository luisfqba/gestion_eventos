<?php
/* @var $this DescuentoController */
/* @var $model Descuento */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'descuento-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'monto'); ?>
		<?php echo $form->textField($model,'monto'); ?>
		<?php echo $form->error($model,'monto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion'); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'porcentage'); ?>
		<?php echo $form->textField($model,'porcentage'); ?>
		<?php echo $form->error($model,'porcentage'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Paquete'); ?>		
                <?php  
                $list = CHtml::listData(Paquete::model()->findAll(array('select'=>'idpaquete, nombre', 'order'=>'nombre')), 'idpaquete', 'nombre');
                echo CHtml::activeDropDownList($model, 'idpaquete', $list, array('empty' => '(Seleccione'));?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->