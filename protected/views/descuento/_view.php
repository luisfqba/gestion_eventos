<?php
/* @var $this DescuentoController */
/* @var $data Descuento */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('iddescuento')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->iddescuento), array('view', 'id'=>$data->iddescuento)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('monto')); ?>:</b>
	<?php echo CHtml::encode($data->monto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('porcentage')); ?>:</b>
	<?php echo CHtml::encode($data->porcentage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idpaquete')); ?>:</b>
	<?php echo CHtml::encode($data->idpaquete); ?>
	<br />


</div>