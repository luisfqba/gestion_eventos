<?php
/* @var $this CuotaController */
/* @var $data Cuota */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idcuota')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idcuota), array('view', 'id'=>$data->idcuota)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('costo')); ?>:</b>
	<?php echo CHtml::encode($data->costo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ocupacion')); ?>:</b>
	<?php echo CHtml::encode($data->ocupacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idpaquete')); ?>:</b>
	<?php echo CHtml::encode($data->idpaquete); ?>
	<br />


</div>