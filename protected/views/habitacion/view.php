<?php
/* @var $this HabitacionController */
/* @var $model Habitacion */

$this->breadcrumbs=array(
	'Habitacions'=>array('index'),
	$model->idhabitacion,
);

$this->menu=array(
	array('label'=>'List Habitacion', 'url'=>array('index')),
	array('label'=>'Create Habitacion', 'url'=>array('create')),
	array('label'=>'Update Habitacion', 'url'=>array('update', 'id'=>$model->idhabitacion)),
	array('label'=>'Delete Habitacion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idhabitacion),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Habitacion', 'url'=>array('admin')),
);
?>

<h1>View Habitacion #<?php echo $model->idhabitacion; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idhabitacion',
		'numero',
		'tipo',
		'costo',
		'maxpersonas',
		'ocupada',
		'idhotel',
	),
)); ?>
