<?php
/* @var $this HabitacionController */
/* @var $model Habitacion */

$this->breadcrumbs=array(
	'Habitacions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Habitacion', 'url'=>array('index')),
	array('label'=>'Manage Habitacion', 'url'=>array('admin')),
);
?>

<h1>Create Habitacion</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>