<?php
/* @var $this HabitacionController */
/* @var $model Habitacion */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'habitacion-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'numero'); ?>
		<?php echo $form->textField($model,'numero'); ?>
		<?php echo $form->error($model,'numero'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo'); ?>
		<?php echo $form->textField($model,'tipo'); ?>
		<?php echo $form->error($model,'tipo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'costo'); ?>
		<?php echo $form->textField($model,'costo'); ?>
		<?php echo $form->error($model,'costo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'maxpersonas'); ?>
		<?php echo $form->textField($model,'maxpersonas'); ?>
		<?php echo $form->error($model,'maxpersonas'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ocupada'); ?>
		<?php echo $form->checkBox($model,'ocupada'); ?>
		<?php echo $form->error($model,'ocupada'); ?>
	</div>
	       
        <div class="row">
		<?php echo $form->labelEx($model,'Hotel'); ?>		
                <?php  
                $list = CHtml::listData(Hotel::model()->findAll(array('select'=>'idhotel, nombre', 'order'=>'nombre')), 'idhotel', 'nombre');
                echo CHtml::activeDropDownList($model, 'idhotel', $list, array('empty' => '(Selecciona un Hotel'));?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->