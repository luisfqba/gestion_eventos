<?php
/* @var $this HabitacionController */
/* @var $model Habitacion */

$this->breadcrumbs=array(
	'Habitacions'=>array('index'),
	$model->idhabitacion=>array('view','id'=>$model->idhabitacion),
	'Update',
);

$this->menu=array(
	array('label'=>'List Habitacion', 'url'=>array('index')),
	array('label'=>'Create Habitacion', 'url'=>array('create')),
	array('label'=>'View Habitacion', 'url'=>array('view', 'id'=>$model->idhabitacion)),
	array('label'=>'Manage Habitacion', 'url'=>array('admin')),
);
?>

<h1>Update Habitacion <?php echo $model->idhabitacion; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>